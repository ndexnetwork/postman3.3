<html>
<head>
    <style>
        .main-block {
            border-radius: 6px;
            box-shadow: 2px 4px 5px rgba(22, 1, 0, 0.4);
            height: auto;
            width: 560px;
            margin: 50px auto;
        }

        .logo {
            border-bottom: 1px solid #03F3F3;
            height: 61px;
            box-shadow: 2px 4px 5px rgba(22, 1, 0, 0.4);
            line-height: 61px;
            padding-top: 11px;
            text-align: center;
            background: #131F3C;
            border-radius: 6px;
        }

        .logo-img {
            height: 40px;
            width: 175px;
        }

        .content {
            padding: 21px 30px 36px 30px;
            text-align: center;
        }

        .title {
            color: black;
            font-family: Lato;
            font-style: normal;
            font-weight: bold;
            font-size: 14px;
            line-height: 17px;
        }

        .text {
            color: black;
            font-family: Lato;
            font-style: normal;
            font-weight: normal;
            font-size: 14px;
            line-height: 17px;
            margin-bottom: 37px;
        }
        .footer
           {
            font-family: Lato Light;
            color: #ffffff;
            box-shadow: 2px 4px 5px rgba(22, 1, 0, 0.4);
            padding: 21px 30px 36px 30px;
            background: #131F3C;
            font-style: normal;
            font-weight: normal;
            font-size: 14px;
            margin-bottom: 37px;
            line-height: 17px;
            margin-top: 37px;
            border-radius: 6px;
            border-top: 2px; 
            }
         .confirmation-button {
            background: #3C78E0;
            margin-left: 172px;
            height: 40px;
            width: 163px;
        }
          
        a {
            color: #FFFFFF;
            font-family: Roboto;
            font-style: normal;
            font-weight: normal;
            font-size: 15px;
            line-height: 35px;
            display: inline-block;
            align-items: center;
            text-align: center;
            text-decoration: none;
           
            }
    </style>
</head>

<body>
    <div class="main-block">
        <div class="logo">
             <img class="logo-img" src="https://raw.githubusercontent.com/QuantaPay/QuantaEx/master/assets/exchange/logo.png" />
        </div>
        <div class="content">
            <p class="title">Welcome to QuantaEx.</p>
            <p class="text">
                Use this unique link to confirm your email {{ .record.user.email }}
            </p>
            <div class="confirmation-button">
                <a href="{{ .record.domain }}/accounts/confirmation?confirmation_token={{ .record.token }}&lang=en">Confirm</a>
            </div>
        </div>
 <p class="footer"> Thank you for visiting <a href="https://www.quantaex.com">https://www.quantaex.com</a>. For support please visit <a href="https://support.quantaex.com">Support Centre</a></p>
    </div>
</body>

</html>
